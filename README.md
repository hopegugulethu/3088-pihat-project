# 3088 PiHat Project

This is a ReSpeaker 2-Mics PiHat that serves as an expansion board for a raspberry pi zero. The PiHat uses 2 microphones to get user audio input for a pedestrian traffic light that is voice activated instead of using the traditional button.

## Table of contents
* [General info](#general-info)
* [Author](#author)
* [License](#license)

## General info
This project is a for a PCB design that extends a raspberry pi.

## Author
* Hope Ngwenya

## License
This project is licensed under GNU General Public License version 3 - see LICENSE.md for details.
